// SPDX-License-Identifier: GPL-2.0-or-later
/*
 *  HID driver for the microsoft ergonomic keyboard
 *
 *  Copyright (c) 2021 Arne Keller
 */

#include <linux/device.h>
#include <linux/input.h>
#include <linux/hid.h>
#include <linux/module.h>

#define USB_VENDOR_ID_MICROSOFT 0x045e
#define USB_DEVICE_ID_MS_ERGONOMIC_KEYBOARD 0x082c
#define MS_ERGONOMY2 BIT(8)

struct ms_data {
	unsigned long quirks;
};


static int ms_raw_event(struct hid_device *hdev, struct hid_report *report,
			u8 *data, int size)
{
	struct ms_data *ms = hid_get_drvdata(hdev);
	unsigned long quirks = ms->quirks;

	if (!(hdev->claimed & HID_CLAIMED_INPUT))
		return 0;

	if (quirks & MS_ERGONOMY2) {
		if (size == 8 && ((data[1] & 0x0f) == 0x0f)) {
			// the weird Office button simulates Left Shift + Left Ctrl + Left Alt + Left Meta..
			data[1] |= 0x80; // instead set Right Meta
			data[1] &= 0xf0; // ..and clear the previous mess
		}
	}
	return 0;
}

static int ms_probe(struct hid_device *hdev, const struct hid_device_id *id)
{
	unsigned long quirks = id->driver_data;
	struct ms_data *ms;
	int ret;

	ms = devm_kzalloc(&hdev->dev, sizeof(*ms), GFP_KERNEL);
	if (ms == NULL)
		return -ENOMEM;

	ms->quirks = quirks;

	hid_set_drvdata(hdev, ms);

	ret = hid_parse(hdev);
	if (ret) {
		hid_err(hdev, "parse failed\n");
		goto err_free;
	}

	ret = hid_hw_start(hdev, HID_CONNECT_DEFAULT);
	if (ret) {
		hid_err(hdev, "hw start failed\n");
		goto err_free;
	}

	return 0;
err_free:
	devm_kfree(&hdev->dev, ms);
	return ret;
}

static void ms_remove(struct hid_device *hdev)
{
	hid_hw_stop(hdev);
}

static const struct hid_device_id ms_devices[] = {
	{ HID_USB_DEVICE(USB_VENDOR_ID_MICROSOFT, USB_DEVICE_ID_MS_ERGONOMIC_KEYBOARD),
	 	.driver_data = MS_ERGONOMY2},
	{ }
};
MODULE_DEVICE_TABLE(hid, ms_devices);

static struct hid_driver ms_driver = {
	.name = "microsoft_ergonomic",
	.id_table = ms_devices,
	.raw_event = ms_raw_event,
	.probe = ms_probe,
	.remove = ms_remove,
};
module_hid_driver(ms_driver);

MODULE_LICENSE("GPL");

